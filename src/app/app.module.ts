import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { AuthenticacionService } from './services/authenticacion.service';
import { APP_ROUTING } from './app.routes';
import { HomeComponent } from './components/home/home.component';
import { ChartsModule } from 'ng2-charts';
import { BarraChartComponent } from './components/barra-chart/barra-chart.component';
import { UploadService } from './services/upload.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BarraChartComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    APP_ROUTING,
    ChartsModule
  ],
  providers: [
    AuthenticacionService,
    UploadService  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
