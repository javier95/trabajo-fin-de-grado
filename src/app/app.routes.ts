import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { BarraChartComponent } from './components/barra-chart/barra-chart.component';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'grafico/:id', component: BarraChartComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});
