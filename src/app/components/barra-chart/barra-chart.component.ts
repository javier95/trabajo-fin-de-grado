import { Component,Injectable } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { Data } from 'src/app/services/Data';
import { UploadService } from 'src/app/services/upload.service';
import { delay } from 'q';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-barra-chart',
  templateUrl: './barra-chart.component.html',
  styleUrls: ['./barra-chart.component.css']
})

export class BarraChartComponent {
  grupo:number;
  barChartData: ChartDataSets[] = [{ data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], label: 'Series A' }];
  datos: Data[] = [];
  barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  barChartLabels: Label[] = ['1', '2', '4', '3', '5', '6', '7', '8', '9', '10'];
  barChartType: ChartType = 'bar';
  barChartLegend = true;



  constructor(private service: UploadService,private _activatedRoute: ActivatedRoute) { 

    this._activatedRoute.params.subscribe(params => {
      this.grupo = params['id'];
    })


  }

  async ngOnInit() {
    console.log("Componente del grafico:" + this.grupo)
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[0] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[1] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[2] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[3] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[4] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[5] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[6] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[7] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[8] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[9] = new Data(result.temperature[0].value);
    });
    await delay(5000);
    this.service.uploadTemperature(this.grupo).subscribe(result => {
      this.datos[10] = new Data(result.temperature[0].value);
    });
    console.log(this.datos);
    let array: number[] = [];
    for (var i = 0; i < this.datos.length; i++) {
      array[i] = this.datos[i].temperature;
    }
    this.barChartData = [{ data: array, label: 'Series A' }];
   }

  async upload() {
    
  }


}
