import { Component, OnInit } from '@angular/core';
import { Router,  } from '@angular/router';
import {UploadService} from 'src/app/services/upload.service'
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
   grupos:number[]=[1,2,3,4,5,6];
   constructor( private _router:Router) { }

   ngOnInit() { }

  upload(idx:number) {
    console.log("Componente home:" + idx);
    this._router.navigate( ['/grafico', idx] );   
  }



}
