import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthenticacionService {

  tokenGrupo1 = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJmamdhcmNpYS5hbHZhcmV6QGhvdG1haWwuY29tIiwic2NvcGVzIjpbIlRFTkFOVF9BRE1JTiJdLCJ1c2VySWQiOiJkNjA2YWU1MC1lYWQwLTExZTktYmM3M"
    + "i1lOWQyMzA2NTUzOTYiLCJmaXJzdE5hbWUiOiJGcmFuY2lzY28gSmF2aWVyIiwibGFzdE5hbWUiOiJHYXJjaWEgQWx2YXJleiIsImVuYWJsZWQiOnRydWUsInByaXZhY3lQb2xpY3lBY2NlcHRlZCI6dHJ1ZSwiaXNQ"
    + "dWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6ImQ1ODA4ZTYwLWVhZDAtMTFlOS1iYzcyLWU5ZDIzMDY1NTM5NiIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ"
    + "0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTU3MTQ5NDQ0NCwiZXhwIjoxNTczMjk0NDQ0fQ.dasLzKVAXIotM5DUKT32RU-fSwK01NGwz_gngojIx4KI-JpU9bpeERQCBdGzOV5LAGtNKdGmDkM6emQvCkUQAA"
  tokenGrupo2;
  tokenGrupo3;
  tokenGrupo4;
  tokenGrupo5;
  tokenGrupo6;
  constructor() { }

  getToken(grupo: number): string {
    console.log("Estoy en el servicio de autenticacion:" + grupo)
    let token: string;
    let numero: number = grupo
    switch (numero) {
      case 0:
        console.log("Estoy en el caso 0");
        token = this.tokenGrupo1;
        break;
      case 1:
        token = this.tokenGrupo2;
        break;
      case 2:
        token = this.tokenGrupo3;
        break;
      case 3:
        token = this.tokenGrupo4;
        break;
      case 4:
        token = this.tokenGrupo5;
        break;
      case 5:
        token = this.tokenGrupo6;
        break;
      default: 
      token = this.tokenGrupo1;
        break;
    }
    return token;
  }

}
