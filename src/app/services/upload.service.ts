import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Data } from './Data';
import { AuthenticacionService } from './authenticacion.service'


@Injectable({
  providedIn: 'root'
})
export class UploadService {
  private endPoint: string;
  data: Data;
 
  constructor(private _http: HttpClient, private service: AuthenticacionService) { }
  uploadTemperature(grupo:number): any {
    console.log("Servicio Upload:" +  grupo);
    console.log("TOKEN:" + this.service.getToken(grupo));
    let header = new HttpHeaders({ 'X-Authorization': "Bearer " + this.service.getToken(grupo)});
    
    let idDevice:string;
    switch(grupo){
      case 0: idDevice = "1b709d30-f598-11e9-9790-7f438c0cbd6e";
      break;
      case 1: idDevice = "";
      break;
      case 2: idDevice = "";
      break;
      case 3: idDevice = "";
      break;
      case 4: idDevice = "";
      break;
      case 5: idDevice = "";
    }
    this.endPoint = "https://demo.thingsboard.io/api/plugins/telemetry/DEVICE/".concat(idDevice + "/values/timeseries?keys=temperature");
    return this._http.get(this.endPoint, { headers: header });
  }
  uploadHumidity(grupo:number): any {
    let header = new HttpHeaders({ 'X-Authorization': "Bearer " + this.service.getToken(grupo)});
    let idDevice:string;
    switch(grupo){
      case 0: idDevice = "1b709d30-f598-11e9-9790-7f438c0cbd6e";
      break;
      case 1: idDevice = "";
      break;
      case 2: idDevice = "";
      break;
      case 3: idDevice = "";
      break;
      case 4: idDevice = "";
      break;
      case 5: idDevice = "";
    }
    this.endPoint = "https://demo.thingsboard.io/api/plugins/telemetry/DEVICE/b6c60e30-eb68-11e9-a68c-7f438c0cbd6e/values/timeseries?keys=humidity";
    return this._http.get(this.endPoint, { headers: header });
  }


}
